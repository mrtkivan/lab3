package com.example.lab3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        val registerButton = findViewById<Button>(R.id.register)
        registerButton.setOnClickListener{
            registerNewUser();
        }
    }

    fun registerNewUser(){
        Log.println(Log.DEBUG,"debug", "registerNewUser");
        val name = findViewById<EditText>(R.id.editTextName).text.toString().trim();
        val surname = findViewById<EditText>(R.id.editTextSurname).text.toString().trim();
        val login = findViewById<EditText>(R.id.editTextLogin).text.toString().trim();
        val password = findViewById<EditText>(R.id.editTextPassword).text.toString().trim();
        val email = findViewById<EditText>(R.id.editTextEmail).text.toString().trim();

        RetrofitClient.instance.createUser(email, name, surname, login, password)
            .enqueue(object: Callback<DefaultResponse>{
                override fun onFailure(call: Call<DefaultResponse>, t: Throwable) {
                    Toast.makeText(applicationContext, t.message, Toast.LENGTH_LONG).show()
                }

                override fun onResponse(call: Call<DefaultResponse>, response: Response<DefaultResponse>) {
                    Toast.makeText(applicationContext, response.body()?.message, Toast.LENGTH_LONG).show()
                }

            })
    }
}