package com.example.lab3

import com.google.gson.annotations.SerializedName

data class DefaultResponse(val error: Boolean, val message:String)