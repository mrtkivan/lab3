package com.example.lab3

data class LoginResponse(val error: Boolean, val message:String, val user: User)