package com.example.lab3

data class User(val id:Int, val email:String, val name:String, val surname:String, val login:String, val password:String)